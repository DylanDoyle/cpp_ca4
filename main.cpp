#include <iostream>
#include <fstream>
#include <string>
#include "Bug.h"
#include "sstream"
#include "Crawler.h"
#include "Hopper.h"

using namespace std;

void displayMenu();


void loadData();
void printBugs();
void findBug(int idToFind);
void moveBugs();
void printPaths();
void writeBugsToFile();
void displayBoard();

vector<Bug*> bugsList;
int main()
{
    bool isRunning = true;
    loadData();
    while(isRunning)
    {
        cout << "Choose an option: " << endl;
        displayMenu();
        int choice;
        cin >> choice;
        if(choice > 0 && choice < 7)
        {
            switch(choice)
            {
                case 1:
                {
                    printBugs();
                    break;
                }
                case 2:
                {
                    cout << "Enter a bug ID to search for: ";
                    int idToFind;
                    cin >> idToFind;
                    findBug(idToFind);
                    break;
                }
                case 3:
                {
                    moveBugs();
                    break;
                }
                case 4:
                {
                    printPaths();
                    break;
                }
                case 5: {

                    displayBoard();
                    break;
                }
                case 6:
                {
                    isRunning = false;
                    writeBugsToFile();
                    cout << "Goodbye!" << endl;
                    break;
                }
            }
        }
    }
}

void loadData()
{
    ifstream fileStream("bugs.txt");
    if(fileStream.good())
    {
        string bugStr;
        while(getline(fileStream, bugStr))
        {
            stringstream bugType(bugStr);
            stringstream line(bugStr.substr(2));
            string bugTypeIndicator = bugType.str();
            if(bugTypeIndicator.at(0) == 'C')
            {
                Crawler* crawlerPtr = new Crawler;
                line >> *crawlerPtr;
                bugsList.push_back(crawlerPtr);
            }
            else if(bugTypeIndicator.at(0) == 'H')
            {
                Hopper* hopperPtr = new Hopper;
                line >> *hopperPtr;
                bugsList.push_back(hopperPtr);
            }
        }
    }
    else
    {
        cout << "Loading from file failed." << endl;
    }
}

void displayMenu()
{
    cout << "1: Display All Bugs\n2: Find a bug\n3: Tap Board\n4: Display Bug History\n5: Display "
            "Board\n6: Exit" << endl;
}

void printBugs()
{
    for(Bug* bug : bugsList)
    {
        cout << *bug << endl;
    }
}

void findBug(int idToFind)
{
    for (Bug* bug : bugsList)
    {
        if(idToFind == bug->getId())
        {
            cout << *bug << endl;
            return;
        }
    }
    cout << "Bug not found!" << endl;
}

void moveBugs()
{
    cout << "Bugs Scurrying..." << endl;
    for(Bug* bug : bugsList)
    {
        if(bug->isBlocked())
        {
            bug->changeDirection();
        }
        else
        {
            bug->move();
            bug->storePaths();
        }
    }

    for(Bug* bug1 : bugsList)
    {
        for(Bug* bug2 : bugsList)
        {
            if(bug1->isAlive())
            {
                if(bug2->isAlive())
                {
                    if(bug1 != bug2)
                    {
                        bug1->fight(*bug2);
                    }
                }
            }
        }
    }
}

void printPaths()
{
    vector <pair<int, int>> bugPath;
    string alive;
    for(Bug* bug : bugsList)
    {
        if(bug->isAlive())
        {
            alive = "Alive";
        }
        else
        {
            alive = "Dead";
        }
        bugPath = bug->getPath();
        cout << bug->getId();
        for(pair<int, int> pos : bugPath)
        {
            cout << "("<< pos.first << "," << pos.second << ")";
        }
        cout << alive << endl;
    }
}

void writeBugsToFile()
{
    vector <pair<int, int>> bugPath;
    ofstream file;
    file.open("bugs_life.txt");
    for(Bug* bug : bugsList)
    {
        bugPath = bug->getPath();
        string alive;
        file << bug->getId();
        for(pair<int, int> pos : bugPath)
        {
            if(bug->isAlive())
            {
                alive = "Alive";
            }
            else
            {
                alive = "Dead";
            }
            file << "("<< pos.first << "," << pos.second << ")";
        }
        file << alive << endl;
    }
    file.close();
}

void displayBoard()
{
    bool bugFound = false;
    for(int x = 0;x <= 9;x++)
    {
        for(int y = 0;y <= 9;y++)
        {
            for(Bug* bug : bugsList)
            {
                if(bug->getPosition().first == x && bug->getPosition().second == y)
                {
                    cout << "(" << x << "," << y <<  ") " << *bug << endl;
                    bugFound = true;
                }
            }
            if(!bugFound)
            {
                cout << "(" << x << "," << y <<  ")" << " EMPTY" << endl;
            }
            bugFound = false;
        }
    }
}

