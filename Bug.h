//
// Created by Dylan 2 on 10/05/2020.
//


#include <utility>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

#ifndef UNTITLED1_BUG_H
#define UNTITLED1_BUG_H

using namespace std;

class Bug
{
protected:
    int id;
    pair<int, int> position;
    int direction;
    int size;
    bool alive;
    vector<pair<int, int>> path;
public:
    virtual void show() = 0;

    Bug(int id, pair<int, int> position, int direction, int size, bool alive, vector<pair<int, int>> path);

    Bug::Bug(Bug const &otherBug);

    Bug();

    int getId() const;

    int getDirection() const;

    int getSize() const;

    bool isAlive() const;

    pair<int, int> getPosition() const;

    vector<pair<int,int>> getPath() const;

    friend ostream &operator<<(ostream &os, Bug &bug);

    virtual void print(ostream &os, Bug &bug);

    virtual void move() = 0;

    bool isBlocked();

    void changeDirection();

    void storePaths();

    string checkDirection();

    void fight(Bug &otherBug);
};







#endif //UNTITLED1_BUG_H
