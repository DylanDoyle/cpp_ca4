//
// Created by Dylan 2 on 10/05/2020.
//

#include "Hopper.h"

Hopper::Hopper(int hopLength, int id, pair<int, int> position, int direction, int size, bool alive,
               vector<pair<int, int>> path) : Bug(id, position, direction, size, alive, path)
{
    this->hopLength = hopLength;
}

Hopper::Hopper(Hopper const &otherHopper)
{
    this->id = otherHopper.id;
    this->path = otherHopper.path;
    this->alive = otherHopper.alive;
    this->size = otherHopper.size;
    this->direction = otherHopper.direction;
    this->position = otherHopper.position;
    this->hopLength = otherHopper.hopLength;
}

int Hopper::getHopLength() const {
    return hopLength;
}

istream &operator >>(istream &is, Hopper &hopper)
{
    string idStr;
    cout << idStr;
    getline(is, idStr, ';');
    try
    {
        int id = stoi(idStr);
        hopper.id = id;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }

    string startXStr;
    cout << startXStr;
    getline(is, startXStr, ';');
    try
    {
        int startX = stoi(startXStr);
        string startYStr;
        getline(is, startYStr, ';');
        int startY = stoi(startYStr);
        pair<int, int> tempPair;
        tempPair.first = startX;
        tempPair.second = startY;
        hopper.position = tempPair;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }

    string directionStr;
    getline(is, directionStr, ';');
    try {
        int direction = stoi(directionStr);
        hopper.direction = direction;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }

    string bugSizeStr;
    getline(is, bugSizeStr, ';');
    try {
        int bugSize = stoi(bugSizeStr);
        hopper.size = bugSize;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }

    hopper.alive = true;

    string hopLengthStr;
    getline(is, hopLengthStr, ';');
    try {
        int hopLength = stoi(hopLengthStr);
        hopper.hopLength = hopLength;
    }
    catch (invalid_argument) {
        cout << "Invalid Argument" << endl;
    }

    return is;
}

Hopper::Hopper() {}

void Hopper::print(ostream &os, Bug &bug)
{
    Bug::print(os, bug);
    os << " Hop Length: " << this->hopLength;
}

void Hopper::move()
{
    if(this->isAlive())
    {
        switch(this->direction) {
            //North/Up
            case 1: {
                this->position.second -= this->hopLength;
                if (this->position.second < 0) {
                    this->position.second = 0;
                }
                break;
            }
                //East/Right
            case 2: {
                this->position.first += this->hopLength;
                if (this->position.first > 9) {
                    this->position.first = 9;
                }
                break;
            }
                //South/Down
            case 3: {
                this->position.second += this->hopLength;
                if (this->position.second > 9) {
                    this->position.second = 9;
                }
                break;
            }
                //West/Left
            case 4: {
                this->position.first -= this->hopLength;
                if (this->position.first < 0) {
                    this->position.first = 0;
                }
                break;
            }
        }
    }
}

