//
// Created by Dylan 2 on 10/05/2020.
//

#ifndef UNTITLED1_CRAWLER_H
#define UNTITLED1_CRAWLER_H
#include "Bug.h"

class Crawler: public Bug
{
public:
    Crawler();

    Crawler(int id, pair<int, int>, int direction, int size, bool alive, vector<pair<int,int>> path);

    Crawler::Crawler(Crawler const &otherCrawler);

    friend ostream &operator<<(ostream &os, Crawler &crawler);

    friend istream &operator>>(istream &is,  Crawler &crawler);

    void show(){};

    void move();
};




#endif //UNTITLED1_CRAWLER_H
