//
// Created by Dylan 2 on 10/05/2020.
//

#include "Crawler.h"
Crawler::Crawler(int id, pair<int, int>position, int direction, int size, bool alive, vector<pair<int,
        int>> path):
        Bug(id, position, direction, size, alive, path)
{}

Crawler::Crawler(const Crawler &otherCrawler) {
    this->id = otherCrawler.id;
    this->path = otherCrawler.path;
    this->alive = otherCrawler.alive;
    this->size = otherCrawler.size;
    this->direction = otherCrawler.direction;
    this->position = otherCrawler.position;
};

istream &operator >>(istream &is, Crawler &crawler)
{
    string idStr;
    getline(is, idStr, ';');
    try
    {
        int id = stoi(idStr);

        crawler.id = id;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }

    string startXStr;
    string startYStr;
    getline(is, startXStr, ';');
    getline(is, startYStr, ';');
    try
    {
        int startX = stoi(startXStr);
        int startY = stoi(startYStr);
        pair<int, int> tempPair;
        tempPair.first = startX;
        tempPair.second = startY;
        crawler.position = tempPair;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }

    string directionStr;
    getline(is, directionStr, ';');
    try {
        int direction = stoi(directionStr);
        crawler.direction = direction;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }


    string bugSizeStr;
    getline(is, bugSizeStr, ';');
    try {
        int bugSize = stoi(bugSizeStr);
        crawler.size = bugSize;
    }
    catch (invalid_argument)
    {
        cout << "Invalid Argument" << endl;
    }
    crawler.alive = true;

    return is;
}

Crawler::Crawler()
{

}

void Crawler::move()
{
    if(this->isAlive())
    {
        switch(this->direction) {
            //North/Up
            case 1: {

                this->position.second -= 1;
                if (this->position.second < 0) {
                    this->position.second = 0;
                }
                break;
            }
                //East/Right
            case 2: {

                this->position.first += 1;
                if (this->position.first > 9) {
                    this->position.first = 9;
                }
                break;
            }
                //South/Down
            case 3: {

                this->position.second += 1;
                if (this->position.second > 9) {
                    this->position.second = 9;
                }
                break;
            }
                //West/Left
            case 4: {

                this->position.first -= 1;
                if (this->position.first < 0) {
                    this->position.first = 9;
                }
                break;
            }
        }
    }
}


