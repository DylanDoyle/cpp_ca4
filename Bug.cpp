//
// Created by Dylan 2 on 10/05/2020.
//

#include "Bug.h"
#include <random>
#include <time.h>

Bug::Bug(int id, const pair<int, int> position, int direction, int size, bool alive, const vector<pair<int,int>> path)
{
    this->id = id;
    this->position = position;
    this->direction = direction;
    this->size = size;
    this->alive = alive;
    this->path = path;
};

Bug::Bug(Bug const &otherBug)
{
    this->id = otherBug.id;
    this->position = otherBug.position;
    this->direction = otherBug.direction;
    this->size = otherBug.size;
    this->alive = otherBug.alive;
    this->path = otherBug.path;
}

Bug::Bug() {}

int Bug::getId() const {
    return id;
}

int Bug::getDirection() const {
    return direction;
}

string Bug::checkDirection()
{
    string directionStr;
    if(direction == 1)
    {
        directionStr = "North";
    }
    else if(direction == 2)
    {
        directionStr = "East";
    }
    else if(direction == 3)
    {
        directionStr = "South";
    }
    else if(direction == 4)
    {
        directionStr = "West";
    }
    else
    {
        directionStr = std::to_string(direction);
    }
    return directionStr;
}

int Bug::getSize() const {
    return size;
}

bool Bug::isAlive() const {
    return alive;
}

pair<int, int> Bug::getPosition() const
{
    return position;
}

vector<pair<int,int>>Bug::getPath() const
{
    return path;
}

ostream &operator<<(ostream &os,Bug &bug)
{
    bug.print(os, bug);
    return os;
}

void Bug::print(ostream &os, Bug &bug)
{
    os << "ID: " << bug.id << " (" << bug.position.first << "," << bug.position.second
       << ") " << bug.direction << " size: " << bug.size;
    if(this->isAlive())
    {
        os << " Alive";
    }
    else
    {
        os << " Dead";
    }
}

bool Bug::isBlocked()
{
    if(this->position.first == 0 && this->direction == 4)
    {
        return true;
    }
    else if(this->position.first == 9 && this->direction == 2)
    {
        return true;
    }
    else if(this->position.second == 0 && this->direction == 1)
    {
        return true;
    }
    else if(this->position.second == 9 && this->direction == 3)
    {
        return true;
    }
    return false;
}

void Bug::changeDirection()
{
    default_random_engine generator(time(NULL));
    uniform_int_distribution<int> distribution(1,4);
    int newDirection;

    while(this->isBlocked())
    {
        newDirection = distribution(generator);
        this->direction = newDirection;
    }
}

void Bug::storePaths()
{
    if(this->isAlive())
    {
        this->path.push_back(this->position);
    }

}

void Bug::fight(Bug &otherBug)
{
    if(this->getPosition().first == otherBug.getPosition().first)
    {
        if(this->getPosition().second == otherBug.getPosition().second)
        {
            if(this->size > otherBug.size)
            {
                otherBug.alive = false;
                this->size += otherBug.size;
            }
            if(this->size < otherBug.size)
            {
                this->alive = false;
                otherBug.size += this->size;
            }
        }
    }
}






