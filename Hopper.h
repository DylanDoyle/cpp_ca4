//
// Created by Dylan 2 on 10/05/2020.
//

#ifndef UNTITLED1_HOPPER_H
#define UNTITLED1_HOPPER_H


#include <iostream>
#include "Bug.h"

class Hopper: public Bug
{
private:
    int hopLength;
public:
    Hopper(int hopLength, int id, pair<int, int> position, int direction,int size,bool alive,
            vector<pair<int, int>> path);

    Hopper::Hopper(Hopper const &otherHopper);

    int getHopLength() const;

    friend ostream &operator<<(ostream &os, Hopper &hopper);

    friend istream &operator>>(istream &is,  Hopper &hopper);

    void show(){};

    Hopper();

    void print(ostream &os, Bug &bug);

    void move();
};



#endif //UNTITLED1_HOPPER_H
